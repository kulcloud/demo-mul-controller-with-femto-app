/*
 *  femto.c: Femto application for MUL Controller 
 *  Copyright (C) 2013, KulCloud Inc.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "config.h"
#include "mul_common.h"
#include "mul_vty.h"
#include "femto.h"

#define FILENAME_TXT "/var/www/cgi-bin/lipa_on.txt"

extern struct mul_app_client_cb femto_app_cbs;
void *thr_fn(void *temp);
uint64_t fm_dpid;
uint64_t sw_dpid;

uint32_t ue_address, up_teid, dl_teid;

void *thr_fn(void *temp) 
{

   uint32_t temp_ip, sipto_gw_ip, sipto_ip, speaker_ip;
   int fd;
   char lipa_on_buf[10];

   int lipa_on_cnt = 0;
   int sipto_on_cnt = 0;
   int block_dup_msg = 0;

   struct flow fl;
   struct flow mask;
   struct mul_act_mdata mdata;

   uint8_t mac[6];

   mac[0] = 0x00;
       mac[1] = 0x15;
       mac[2] = 0x17;
       mac[3] = 0x14;
       mac[4] = 0xC6;
       mac[5] = 0x01;


   memset(&fl, 0, sizeof(fl));
   memset(&mask, 0, sizeof(mask)); 
   
   //temp_ip = "0x0AFD0202";
   sipto_gw_ip = inet_addr("20.1.1.1");
   sipto_ip = inet_addr("128.134.51.167");
   temp_ip = inet_addr("10.253.2.2");
   speaker_ip = inet_addr("10.253.2.50");
   //printf("thread ing...\n");
      
      while(1) {
         //printf("while ing...\n");
         fd = open(FILENAME_TXT, O_RDONLY);
         if(fd < 0) 
         {
            c_log_debug("lipa_on.txt file open fail!!\n");
         }
         read(fd, lipa_on_buf, sizeof(lipa_on_buf));
         //printf("%s\n", lipa_on_buf);
         close(fd);

         if((lipa_on_buf[0] == 'S') && (lipa_on_cnt == 1)) 
         {
            lipa_on_cnt = 0;

            mul_app_send_femto_conf(NULL, NULL, /**(uint64_t *)fm_dpid*/sw_dpid, 0, 0, 0, 0, 0, 0,
                  temp_ip, 0);

            c_log_debug("DEL LIPA IP MESSAGE SEND!!\n");

         } 
         if( (lipa_on_buf[0] == '1') && (lipa_on_cnt == 0) ) 
         {
            mul_app_send_femto_conf(NULL, NULL, sw_dpid, 0, ue_address, 0, 0, 0, 1,
                  temp_ip, 0);
            /*
            mul_app_send_femto_conf(NULL, NULL, *(uint64_t *)sw_dpid, 0, 0, 0, 0, 0, 1,
                  speaker_ip, 0);*/
            
            c_log_debug("LIPA ON MESSAGE SEND!!\n");

            //lipa_on_buf[0] = NULL;
            lipa_on_cnt = 1;
            block_dup_msg = 1;
            
             // Clear all entries for piolink switch
/*         mul_app_send_flow_del(FEMTO_APP_NAME, NULL, sw_dpid, &fl,
            &mask, OFPP_NONE, 0, C_FL_ENT_NOCACHE, OFPG_ANY);
*/
         /* 
            In Piolink Switch Config
            Flow: ethernet_type = 2048, dst_ip = UE, src_ip = Localhost, 
                  inport = Localhost_linked_port
            Action: set_dmac = Femto_MAC, output = Femto_linked_port
         */
/*
         memset(&fl, 0, sizeof(fl));
         of_mask_set_dc_all(&mask);
         of_mask_set_dl_type(&mask);
         of_mask_set_nw_dst(&mask, 32);          // wildcardmask
         of_mask_set_nw_src(&mask, 32);          // wildcardmask
         of_mask_set_in_port(&mask);
         fl.dl_type = htons(ETH_TYPE_IP);        // IPv4: 0x800
         fl.nw_dst = htonl(0xC0A8010F);          // UE's IP
         fl.nw_src = htonl(0x0AFD0202);          // Localhost's IP
         fl.in_port = htonl(0x3);                        // Switch port of localhost

         mul_app_act_alloc(&mdata);
         mul_app_act_set_ctors(&mdata, sw_dpid);
         mul_app_action_set_dmac(&mdata, mac);   // dst_femto_mac
         mul_app_action_output(&mdata, 0x2);             // femto_linked_port
         mul_app_send_flow_add(FEMTO_APP_NAME, NULL, sw_dpid, &fl,
                                                   &mask, 0xffffffff,
                                                   mdata.act_base, mul_app_act_len(&mdata),
                                                   0, 0, C_FL_PRIO_DFL, 0);
         mul_app_act_free(&mdata);
         c_log_debug("set flow to switch 0x%llx ", (unsigned long long)(sw_dpid));
*/         
         }
         else if((lipa_on_buf[0] == 'S') && (lipa_on_cnt == 1))
         {
            lipa_on_cnt = 0;

            mul_app_send_femto_conf(NULL, NULL, /**(uint64_t *)fm_dpid*/sw_dpid, 0, 0, 0, 0, 0, 0,
                  temp_ip, 0);

            c_log_debug("DEL LIPA IP MESSAGE SEND!!\n");

         }
         else if((lipa_on_buf[1] == 'i') && (sipto_on_cnt == 1)) 
         {
/*            sipto_on_cnt = 0;
            mul_app_send_femto_conf(NULL, NULL, *(uint64_t *)sw_dpid, 0, sipto_gw_ip, 
               0, sipto_ip, 0, 0, 0, 0);
*/            c_log_debug("DEL SIPTO IP MESSAGE SEND!!");
         } 
         else if((lipa_on_buf[1] == '1') && (sipto_on_cnt == 0) ) 
         {
            mul_app_send_femto_conf(NULL, NULL, *(uint64_t *)sw_dpid, 1, sipto_gw_ip,
               0, sipto_ip, 0, 0, 0, 0);
            c_log_debug("SIPTO ON MESSAGE SEND!!\n");
            //lipa_on_buf[1] == NULL;
            sipto_on_cnt = 1;
         } 
         else 
         {
            block_dup_msg = 0;
         }
      } 

}

static void 
femto_add(mul_switch_t *sw) 
{
   uint32_t temp_ip;

   int fd;
   char lipa_on_buf[10];
   int lipa_on_cnt = 0;
   int a =4;
   int thread_id;

   pthread_t p_thread[2];


   //sw_dpid = sw->dpid;
   //c_log_debug("Femto Switch 0x%llx added", (unsigned long long)(sw->dpid));

   if((sw->dpid) == 0x1100000000) {
      c_log_debug("Femto Agent 0x%llx added", (unsigned long long)(sw->dpid));
      fm_dpid = sw->dpid;
       
      /*thread_id = pthread_create(&p_thread[0], NULL, thr_fn, (void *)&fm_dpid); 
         if(thread_id != 0) {
            c_log_debug("thread error!!\n");
         }*/
   } else {
      sw_dpid = sw->dpid;

      thread_id = pthread_create(&p_thread[0], NULL, thr_fn, (void *)&sw_dpid);
         if(thread_id != 0) {
            c_log_debug("thread error!!\n");
         }
      //printf("main thread ing ...\n");

      c_log_debug("Femto Switch 0x%llx added", (unsigned long long)(sw->dpid));
   }
   
}

static void
femto_del(mul_switch_t *sw) {
   c_log_debug("Femto Switch 0x%llx removed", (unsigned long long)(sw->dpid));
}


/* Vendor message - This is only for TEST */
struct c_ofp_rf_dp {
    uint32_t                   bearer_id;
    uint32_t                   user_id;
    uint64_t                   rf_value;
};

static void
femto_vendor_msg(mul_switch_t *sw, uint8_t *msg, size_t pkt_len) 
{
   
   struct ofp_vendor_specific_msg *ofpv_msg = (void *)msg;
   //struct vendor_specific_header *vendor_hdr;

   struct flow fl;
   struct flow mask;
   struct mul_act_mdata mdata;
/*
   uint8_t mac[6];

       mac[0] = 0x00;
       mac[1] = 0x15;
       mac[2] = 0x17;
       mac[3] = 0x14;
       mac[4] = 0xC6;
       mac[5] = 0x01;

   memset(&fl, 0, sizeof(fl));
   memset(&mask, 0, sizeof(mask));
*/
   c_log_debug("FEMTO GET MSG !! pkt_len : %d ", pkt_len);
   
   c_hex_dump(msg, pkt_len);

   if(!ofpv_msg) {
      c_log_err("%s: Invaild arg", FN);
      return;
   }

   //temp_ip = "0x0AFD0202";
/*
   do {
      fd = open(FILENAME_TXT, O_RDONLY);
      if(fd < 0) {
        c_log_debug("lipa_on.txt file open fail!!\n");
      }
      read(fd, lipa_on_buf, sizeof(lipa_on_buf));
      //printf("%s\n", lipa_on_buf);
      close(fd);

      if(lipa_on_buf[0] == '1') {
         c_log_debug("LIPA ON MESSAGE SEND!!\n");
         mul_app_send_femto_conf(NULL, NULL, sw->dpid, 0, 0, 0, 0, 0, 0,
               temp_ip, 0);
         lipa_on_buf[0] = NULL;
         lipa_on_cnt = 1;
      }
   } while(lipa_on_cnt != 1);
   
   fd = open(FILENAME_TXT, O_RDONLY);
   if(fd < 0) {
     c_log_debug("lipa_on.txt file open fail!!\n");
   }
   read(fd, lipa_on_buf, sizeof(lipa_on_buf));
   close(fd);

   if(lipa_on_buf[0] == 1) {
      c_log_debug("LIPA ON MESSAGE SEND!!\n");
   }
*/
   //vendor_hdr = (void *)(ofpv_msg);
    
   switch(htons(ofpv_msg->header.type)) {
      case OFPVSMT_FEMTO_CONFIGURATION_MESSAGE : {
         
         struct ofp_vendor_femto_config_msg *ofpv_femto_cfg_msg;
         //ofpv_femto_cfg_msg = (void *)(vendor_hdr);
         c_log_debug("%s: OFPVSMT_FEMTO_CONFIGURATION_MESSAGE", FN);
         break;
      }

      case OFPVSMT_FEMTO_STATUS_REPORT : 
      {

/*         struct ofp_vendor_status_report *ofpv_status_rpt = 
            (struct ofp_vendor_status_report *)(ofpv_msg + 1);
*/         
         struct ofp_vendor_status_report *ofpv_status_rpt;
         ofpv_status_rpt = (void *)(ofpv_msg->body);
         c_log_debug("%s: OFPVSMT_FEMTO_STATUS_REPORT", FN);
         c_log_debug("%x : ofpv_status_rpt->event", ofpv_status_rpt->event);
         switch(ofpv_status_rpt->event) 
         {
         
            case VSSRE_TERMINAL_ENTRANCE : 
            {
            
               struct event_terminal_entrance_report *event_enter_rpt;
               event_enter_rpt = (void *)(ofpv_status_rpt->body);
               
               c_log_debug("%x %x %x ", ntohl(event_enter_rpt->imsi), 
                  ntohl(event_enter_rpt->bearer_status), 
                  ntohl(event_enter_rpt->terminal_ip));

               up_teid = htonl(event_enter_rpt->imsi);
               dl_teid = htonl(event_enter_rpt->bearer_status);
               ue_address = htonl(event_enter_rpt->terminal_ip);
               c_log_debug("ue address : %x uplink_teid : %x downlink_teid : %x ", 
                  ue_address, up_teid, dl_teid);           

               mul_app_send_femto_conf(NULL, NULL, sw_dpid, 0, ue_address, 
                  0, up_teid, 0, 5, dl_teid, 0);
               c_log_debug("send UE enter information to switch!!");
               break;
            }

            case VSSRE_TERMINAL_LEAVE : 
            {

               c_log_debug("%s: VSSRE_TERMINAL_LEAVE", FN);
               struct event_terminal_leave_report *event_leave_rpt;
               event_leave_rpt = (void *)(ofpv_status_rpt->body);

               
               c_log_err("%s: 0x%x vendor terminal leave", 
                  FN, ntohl(event_leave_rpt->imsi));
               ue_address = htonl(event_leave_rpt->imsi);
               mul_app_send_femto_conf(NULL, NULL, sw_dpid, 0, ue_address,
                  0, 0, 0, 6, 0, 0);
               c_log_debug("send UE leave information to switch!!");
              /* mul_app_send_flow_del(FEMTO_APP_NAME, NULL, sw_dpid, &fl, &mask, 
                  FEMTO_UNK_BUFFER_ID, 0, C_FL_ENT_NOCACHE, OFPG_ANY);
               c_log_debug("delete app send flow!!");*/
               break;
            }

		}
		break;
	    }

	case OFPVSMT_WIFI_CONFIGURATION_MESSAGE:
	    break;

	case OFPVSMT_WIFI_STATUS_REPORT:
	    break;

	case OFPVSMT_SDN_SWITCH_CONFIGURATION_MESSAGE:
	    break;

	default:
	    break;
    }	

}

struct mul_app_client_cb femto_app_cbs = {
    .switch_add_cb =  femto_add,
    .switch_del_cb = femto_del,
    .process_vendor_msg_cb = femto_vendor_msg
};  
/* 
   struct mul_app_client_cb l2sw_app_cbs = {
   .switch_priv_alloc = l2sw_alloc,
   .switch_priv_free = l2sw_free,
   .switch_add_cb =  l2sw_add,
   .switch_del_cb = l2sw_del,
   .switch_priv_port_alloc = NULL,
   .switch_priv_port_free = NULL,
   .switch_port_add_cb = NULL,
   .switch_port_del_cb = NULL,
   .switch_port_link_chg = NULL,
   .switch_port_adm_chg = NULL,
   .switch_packet_in = l2sw_learn_and_fwd,
   .core_conn_closed = l2sw_core_closed,
   .core_conn_reconn = l2sw_core_reconn 
   };  
 */

    void
femto_module_init(void *base_arg)
{
    struct event_base *base = base_arg;
   
    c_log_debug("%s", FN);
   
#if 0
    mul_register_app(NULL, FEMTO_APP_NAME, 
	    C_APP_ALL_SW, C_APP_ALL_EVENTS,
	    0, NULL, femto_event_notifier);
#else
    mul_register_app_cb(NULL, FEMTO_APP_NAME,
	    C_APP_ALL_SW, C_APP_ALL_EVENTS,
	    0, NULL, &femto_app_cbs);
#endif

    return;
}

    void
femto_module_vty_init(void *arg UNUSED)
{
    c_log_debug("%s:", FN);
}

module_init(femto_module_init);
module_vty_init(femto_module_vty_init);

/*
 *  femto.h: Femto application headers
 *  Copyright (C) 2013, KulCloud Inc.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/*
  * OFP Vendor Extension Message
  */

#define FEMTO_UNK_BUFFER_ID (0xffffffff)

struct ofp_vendor {
	struct ofp_header 	header; /* Type OFPT_VENDOR. */
	uint32_t 			vendor; /* Vendor ID:
						* - MSB 0: low-order bytes are IEEE OUI.
						* - MSB != 0: defined by OpenFlow
						* consortium. */
	/* Vendor-defined arbitrary additional data. */
	uint8_t 			message[0]; /* Type - Struct ofp_vendor_specific_msg */
};


/* 
  * Vendor specific Message
  */
  
/* Vendor Message Header */
struct vendor_specific_header {
	uint16_t 	type;  	/*One of the OFPVSMT_* Constants */
	uint16_t	length;
	uint8_t		pad[4];
};

/* Vendor specific message type */
enum ofp_vendor_specific_msg_type {
	OFPVSMT_FEMTO_CONFIGURATION_MESSAGE,
	OFPVSMT_FEMTO_STATUS_REPORT,
	OFPVSMT_WIFI_CONFIGURATION_MESSAGE,
	OFPVSMT_WIFI_STATUS_REPORT,
	OFPVSMT_SDN_SWITCH_CONFIGURATION_MESSAGE
};

struct ofp_vendor_specific_msg {
	struct vendor_specific_header 	header;
	uint8_t 						body[0];	/* Body of the request message */
};


/* 
  * Vendor APIs
  */
  
/* Femto Configuration Message */
struct status_report_criteria {
	uint32_t	onload_report_threshold;
	uint32_t	offload_report_threshold;
};

struct femto_operation_control {
	uint8_t 						lipa_status; 	/* On/OFF */
	uint8_t 						sipto_status; 	/* On/OFF */
	uint8_t							pad[6];
	struct status_report_criteria 	report_criteria;
};

struct ofp_vendor_femto_config_msg {
	struct vendor_specific_header 	header; 
		   /*Type - OFPVSMT_FEMTO_CONFIGURATION_MESSAGE */
	struct femto_operation_control	op_ctrl;
};


/* Femto Status Report */
struct ofp_vendor_status_report {
	struct vendor_specific_header 	header; 
		   /*Type - OFPVSMT_FEMTO_STATUS_REPORT */
	uint8_t		event; 		/*One of the VSSRE_* Constants*/
        uint8_t         pad[1];
	uint8_t 	body[0]; 	/*Body of the report based on event type*/
};


/* Femto Status report events */
enum vendor_specific_status_report_events {
	VSSRE_TERMINAL_ENTRANCE,
	VSSRE_TERMINAL_LEAVE,
	VSSRE_BEARER_STATUS_REPORT,
	VSSRE_CHANNEL_STATUS_REPORT
};


/* Terminal Entrance Event */
struct event_terminal_entrance_report {
	uint32_t 	imsi;
        //uint8_t  guti[13];
	uint32_t	bearer_status;	/* With/Without Bearer*/
	uint32_t	terminal_ip; 	/*Conditional*/
};


/* Terminal leave Event */
struct event_terminal_leave_report {
	uint32_t 	imsi;
};


/* Bearer Status Report Event */
struct event_bearer_status_report_update {
	uint32_t 					imsi;
	uint8_t						bearer_id; 		/* With/Without Bearer*/
	uint8_t 					pad[3];
	//struct bearer_qos_params 	qos_params;
};


/* Channel Status Report Event */
struct event_channel_status_report_update {
	uint32_t 	channel_status; /* one of CHANNEL_STATUS type*/
};

enum channel_status_type {
	CHANNEL_STATUS_OVERLOADED,
	CHANNEL_STATUS_NORMAL,
	CHANNEL_STATUS_PERIODIC
};

void femto_module_init(void *ctx);
void femto_module_vty_init(void *arg);


